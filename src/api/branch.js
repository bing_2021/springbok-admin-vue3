import request from "@/utils/request";

export function pageSysBranch(data) {
  return request({
    url: "/branch/pageSysBranch",
    method: "GET",
    params: data,
  });
}
export function addSysBranch(data) {
  return request({
    url: "/branch/addSysBranch",
    method: "POST",
    data,
  });
}
export function updateSysBranch(data) {
  return request({
    url: "/branch/updateSysBranch",
    method: "POST",
    data,
  });
}
export function deleteSysBranch(sysBranchId) {
  return request({
    url: "/branch/deleteSysBranch",
    method: "POST",
    params: { sysBranchId },
  });
}
export function enabledSysBranch(sysBranchId, branchStatus) {
  return request({
    url: "/branch/enabledSysBranch",
    method: "POST",
    params: { sysBranchId, branchStatus },
  });
}
export function listSysBranch(data) {
  return request({
    url: "/branch/listSysBranch",
    method: "GET",
    params: data,
  });
}